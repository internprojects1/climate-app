import 'package:clima/services/location.dart';
import 'package:clima/services/networking.dart';
import 'package:clima/utilities/constants.dart';

class WeatherModel {
  Future<dynamic> getCityWeather(String cityName) {
    var url =
        'api.openweathermap.org/data/2.5/weather?q=$cityName&appid=c230e66cce35b5dc2c1334b24ba9f5ca';
    NetworkHelper networkHelper = NetworkHelper(url: url);
    var weatherData = networkHelper.getData();
    return weatherData;
  }

  Future<dynamic> getLocationWeather() async {
    Location location = new Location();
    await location.getCurrentLocation();

    String url =
        'http://api.openweathermap.org/data/2.5/weather?lat=10.958621&lon=78.105993&appid=c230e66cce35b5dc2c1334b24ba9f5ca&units=metric';
    //'http://api.openweathermap.org/data/2.5/weather?lat=10.9586921&lon=78.1059093&appid=c230e66cce35b5dc2c1334b24ba9f5ca&units=metric'
    //'http://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&appid=$apiKey&units=metric'
    print('${location.latitude} ${location.longitude} $apiKey');
    NetworkHelper networkHelper = new NetworkHelper(url: url);

    var weatherData = await networkHelper.getData();

    return weatherData;
  }

  String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 25) {
      return 'It\'s 🍦 time';
    } else if (temp > 20) {
      return 'Time for shorts and 👕';
    } else if (temp < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }
}

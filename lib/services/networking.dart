import 'package:http/http.dart' as http;
import 'dart:convert';

class NetworkHelper {
  NetworkHelper({this.url});

  String url;

  Future getData() async {
    http.Response response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      String data = response.body;
      print('data');
      print(data);
      var decodedData = jsonDecode(data);
      print('decodedData');
      print(decodedData);
      return decodedData;
    } else {
      print(response.statusCode);
    }
  }
}
